#ifndef Animator_hpp
#define Animator_hpp

#include <QtCore/QTimer>

class QGLWidget;

class Animator: public QObject {
public:
  Animator(QGLWidget* glWidget);

  void
  start(int milliseconds = 0);

  void
  stop();

private:
  QTimer _timer;
};

#endif