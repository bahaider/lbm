#include "LbmWorker.hpp"

#include <QtCore/QCoreApplication>
#include <QtCore/QString>
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtOpenGL/QGLContext>

#include <QtCore/QDebug>

#include "GpuCLInitialization.hpp"

#include "LoggingMacros.hpp"
LbmWorker::
LbmWorker():
  _densityBufferNumber(-1),
  _updateSharedBuffer(false),
  _started(false),
  _simulationMode(true) {}

LbmWorker::~LbmWorker() {
  PRINT("LbmWorker destroyed");
}

void
LbmWorker::
initialize(QGLContext* glContext) {
  glContext->makeCurrent();

  _solver.clInitialization(
    GpuCLInitialization::createShared());

  _solver.kernelSource(
    QString("%1/kernel.cl").arg(
      QCoreApplication::applicationDirPath()).toStdString().c_str());
  _solver.routineSource(
    QString("%1/routine.cl").arg(
      QCoreApplication::applicationDirPath()).toStdString().c_str());

  _solver.tau(1.0);
  _solver.wallVelocity(2.0);
  _solver.initialize();

  _timer = new QTimer();
  connect(_timer, SIGNAL(timeout()), this, SLOT(process()));
}

void
LbmWorker::
setGLContext() {}

void
LbmWorker::
reset() {
  QMutexLocker locker(&_solverMutex);

  _solver.initialize();
}

void
LbmWorker::
sizeX(int sizeX) {
  _solver.sizeX(sizeX);
}

int
LbmWorker::
sizeX() const {
  return _solver.sizeX();
}

void
LbmWorker::
sizeY(int sizeY) {
  _solver.sizeY(sizeY);
}

int
LbmWorker::
sizeY() const {
  return _solver.sizeY();
}

void
LbmWorker::
sizeZ(int sizeZ) {
  _solver.sizeZ(sizeZ);
}

int
LbmWorker::
sizeZ() const {
  return _solver.sizeZ();
}

void
LbmWorker::
scenarioFilePath(QString const& scenarioFilePath) {
  std::string include = scenarioFilePath.toStdString();
  _solver.includeFiles(include);
}

void
LbmWorker::
setDensityBuffer(
  GLenum target,
  GLuint textureObject) {
  _solver.setDensityBuffer(
    target,
    textureObject);
}

void
LbmWorker::
setVelocityBuffer(
  GLenum target,
  GLuint textureObject) {
  _solver.setVelocityBuffer(
    target,
    textureObject);
}

void
LbmWorker::
updateSharedBuffers() {
  QMutexLocker locker(&_solverMutex);
  _solver.updateDensityBuffer();
  _solver.updateVelocityBuffer();
}

void
LbmWorker::
setFlagBuffer(
  GLenum target,
  GLuint textureObject) {
  _solver.setFlagBuffer(
    target,
    textureObject);
}

void
LbmWorker::
updateFlagBuffer() {
  QMutexLocker locker(&_solverMutex);
  _solver.updateFlagBuffer();
}

void
LbmWorker::
start() {
  if (!_started)
    if (_simulationMode)
      _timer->start();



  if (!_simulationMode)
    process();

  _started = true;
}

void
LbmWorker::
toggleSimulationMode() {
  if (_started)
    if (_timer->isActive())
      _timer->stop();
    else
      _timer->start();

  _simulationMode = !_simulationMode;
  emit simulationModeChanded((int)_simulationMode);
}

void
LbmWorker::
increaseDelay() {
  // _timer->setInterval(1000);
  _timer->setInterval(_timer->interval() + 100);
}

void
LbmWorker::
reduceDelay() {
  if (_timer->interval() >= 100)
    _timer->setInterval(_timer->interval() - 100);
}

void
LbmWorker::
process() {
  _solverMutex.lock();
  _solver.run();
  _solverMutex.unlock();
}

void
LbmWorker::
finish() {
  QMutexLocker locker(&_finishMutex);
  _finish = true;
}

void
LbmWorker::
waitFinish() {
  _finishMutex.lock();
  _finish = true;
  _finishCondition.wait(&_finishMutex);
  _finishMutex.unlock();
}

void
LbmWorker::
pause() {
  _pauseMutex.lock();
  _pause = true;
  _pauseMutex.unlock();
}

void
LbmWorker::
resume() {
  _pauseMutex.lock();
  _pause = false;
  _pauseMutex.unlock();
  _pauseCondition.wakeAll();
}

void
LbmWorker::
updateFinish() {
  QMutexLocker locker(&_finishMutex);
  _finish = false;
}

bool
LbmWorker::
isFinished() {
  QMutexLocker locker(&_finishMutex);
  return _finish;
}

void
LbmWorker::
isPaused() {
  _pauseMutex.lock();

  if (_pause)
    _pauseCondition.wait(&_pauseMutex);

  _pauseMutex.unlock();
}
