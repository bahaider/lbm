#ifndef Console_hpp
#define Console_hpp

#include "Printer.hpp"

#include <QtGui/QTextEdit>

class Console: public QTextEdit, public Printer {
public:
  Console(QWidget* parent = 0);

  void
  print(QString const& string);
};

#endif