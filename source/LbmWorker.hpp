#ifndef Core_LbmWorker_hpp
#define Core_LbmWorker_hpp

#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QWaitCondition>

#include "LbmSolver.hpp"

class QGLContext;

struct Switch {
  Switch():
    _switch(false) {}

  bool
  isOn() {
    QMutexLocker locker(&_mutex);
    return _switch;
  }

  void
  on() {
    QMutexLocker locker(&_mutex);
    _switch = true;
  }

  void
  off() {
    QMutexLocker locker(&_mutex);
    _switch = false;
  }

  void
  waitOn() {
    QMutexLocker locker(&_mutex);
    _switch = true;
    _condition.wait(&_mutex);
  }

  void
  wakeAll() {
    QMutexLocker locker(&_mutex);
    _condition.wakeAll();
  }

private:
  bool           _switch;
  QMutex         _mutex;
  QWaitCondition _condition;
};

class QTimer;
class LbmWorker: public QObject {
  Q_OBJECT

public:
  LbmWorker();

  ~LbmWorker();

public slots:
  void
  initialize(QGLContext* glContext);

  void
  setGLContext();

  void
  reset();

public slots:
  void
  sizeX(int sizeX);

public:
  int
  sizeX() const;

public slots:
  void
  sizeY(int sizeY);

public:
  int
  sizeY() const;

public slots:
  void
  sizeZ(int sizeZ);

public:
  int
  sizeZ() const;

public slots:
  void
  scenarioFilePath(QString const& scenarioFilePath);

public slots:
  void
  setDensityBuffer(
    GLenum target,
    GLuint textureObject);

  void
  setVelocityBuffer(
    GLenum target,
    GLuint textureObject);

  void
  updateSharedBuffers();

  void
  setFlagBuffer(
    GLenum target,
    GLuint textureObject);

  void
  updateFlagBuffer();

public slots:
  void
  process();

  void
  start();

  void
  toggleSimulationMode();

signals:
  void
  simulationModeChanded(int mode);

public slots:
  void
  increaseDelay();

  void
  reduceDelay();

  void
  finish();

  void
  waitFinish();

  void
  pause();

  void
  resume();

signals:
  void
  finished();

private:
  void
  updateFinish();

  bool
  isFinished();

  void
  isPaused();

private:
  LbmSolver _solver;

  bool           _finish;
  QMutex         _finishMutex;
  QWaitCondition _finishCondition;

  bool           _pause;
  QMutex         _pauseMutex;
  QWaitCondition _pauseCondition;

  int            _densityBufferNumber;
  QMutex         _densityBufferMutex;
  QWaitCondition _densityBufferCondition;

  bool           _updateSharedBuffer;
  QMutex         _sharedBufferMutex;
  QWaitCondition _sharedBufferCondition;

  bool _started;
  bool _simulationMode;

  QTimer* _timer;

  QMutex _solverMutex;
};

#endif /* ----- end Core_LbmWorker_hpp ----- */
