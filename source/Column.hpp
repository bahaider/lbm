#ifndef Column_hpp
#define Column_hpp

#include <Eigen/Geometry>

namespace Math {
template <class T, int size>
using Column = Eigen::Matrix<T, size, 1>;

template <class T>
using Column3 = Column<T, 3>;

template <class T>
using Column4 = Column<T, 4>;
}

#endif