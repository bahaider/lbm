#ifndef MouseEvent_hpp
#define MouseEvent_hpp

#include "Button.hpp"

class MouseEvent {
public:
  MouseEvent(Button button,
             int    x,
             int    y,
             int    width,
             int    height,
             int    globalX,
             int    globalY,
             int    globalWidth,
             int    globalHeight);

  Button
  button() const;

  int
  originX() const;

  int
  originY() const;

  int
  x() const;

  int
  y() const;

  int
  width() const;

  int
  height() const;

  int
  globalX() const;

  int
  globalY() const;

  int
  globalWidth() const;

  int
  globalHeight() const;

private:
  Button _button;

  int _x;
  int _y;

  int _width;
  int _height;

  int _globalX;
  int _globalY;

  int _globalWidth;
  int _globalHeight;
};

#endif