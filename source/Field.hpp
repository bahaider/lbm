#ifndef Field_hpp
#define Field_hpp

#include <GL/glew.h>

#include <oglplus/all.hpp>

class Field {
public:
  Field(GLuint                           sampleCountX,
        GLuint                           sampleCountY,
        GLuint                           sampleCountZ,
        oglplus::PixelDataInternalFormat internalFormat,
        oglplus::PixelDataFormat         format,
        oglplus::PixelDataType           type,
        oglplus::TextureMinFilter        minFilter,
        oglplus::TextureMagFilter        magFilter);

  Field(Field const& other) = delete;

  Field const&
  operator=(Field const& other) = delete;

  virtual
  ~Field() = 0;

  GLuint
  sampleCountX() const;

  GLuint
  sampleCountY() const;

  GLuint
  sampleCountZ() const;

  GLuint
  texelCountX() const;

  GLuint
  texelCountY() const;

  GLuint
  texelCountZ() const;

  void
  attach() const;

  void
  attach(GLuint textureUnit) const;

  void
  detach() const;

  void
  detach(GLuint textureUnit) const;

  GLuint
  expose() const;

protected:
  oglplus::Texture     _texture;
  oglplus::Framebuffer _frameBuffer;

  GLuint _sampleCountX;
  GLuint _sampleCountY;
  GLuint _sampleCountZ;

  GLuint _texelCountX;
  GLuint _texelCountY;
  GLuint _texelCountZ;
};
#endif