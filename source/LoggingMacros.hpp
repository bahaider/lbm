#ifndef LoggingMacros_hpp
#define LoggingMacros_hpp

#include <cstdio>
#include <cstring>
#include <string>

namespace Detail {
template <typename T>
inline char const*
convert(T const& t) {
  return t;
} /* ----- end of convert ----- */

template <>
inline char const* convert<
  std::string
  >(std::string const& t) {
  return t.c_str();
} /* ----- end of convert ----- */
} /* ----- end of Detail ----- */

#define PRINT(x, ...)                                           \
  if (true) {                                                   \
    char const* message = Detail::convert(x);                   \
    if (message == 0) {                                         \
      std::printf("%s:%d |  %s\n", __FILE__, __LINE__, "");     \
    }                                                           \
    else {                                                      \
      int length = std::snprintf(0,                             \
                                 0,                             \
                                 message,                       \
                                 ## __VA_ARGS__);               \
      ++length;                                                 \
      char* buffer = new char[length];                          \
      std::snprintf(buffer, length, message, ## __VA_ARGS__);   \
      std::printf("%s:%d |  %s\n", __FILE__, __LINE__, buffer); \
      delete buffer;                                            \
    }                                                           \
  }

#define FATAL(x, ...)         \
  if (true) {                 \
    PRINT(x, ## __VA_ARGS__); \
    exit(1);                  \
  }

#endif /* ----- end LoggingMacros_hpp ----- */
