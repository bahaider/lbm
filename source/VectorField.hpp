#ifndef VectorField_hpp
#define VectorField_hpp

#include "Field.hpp"

class VectorField: public Field {
public:
  VectorField(GLuint sampleCountX,
              GLuint sampleCountY,
              GLuint sampleCountZ);
};

#endif
