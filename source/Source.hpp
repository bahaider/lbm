#ifndef Source_hpp
#define Source_hpp

#include <string>

class Source {
public:
  Source();

  Source(
      char const* filePath);

  Source(
    Source const& other);
  virtual
  ~Source();

  Source const&
  operator=(
    Source const& other);

  ::size_t
  length() const;

  char const*
  get() const;

private:
  std::string _source;
}; /* ----- end of Source ----- */

#endif
