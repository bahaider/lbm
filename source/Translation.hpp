#ifndef Translation_hpp
#define Translation_hpp

#include <Eigen/Geometry>

namespace Math {
template <class T, int size>
using Translation = Eigen::Translation<T, size>;

template <class T>
using Translation3 = Translation<T, 3>;
}

#endif