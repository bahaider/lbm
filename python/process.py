import subprocess
import sys


def call(args):
    if sys.platform.startswith('win'):
        shell = True
    else:
        shell = False

    subprocess.call(args, shell=shell)
